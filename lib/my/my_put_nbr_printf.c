/*
** EPITECH PROJECT, 2017
** emacs
** File description:
** my_put_nbr.c
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "my.h"

int	my_putnbr_p(va_list ap)
{
	int	nb = va_arg(ap, int);
	int	i = 0;
	char	*nbr = malloc(sizeof(char) * 14);

	if (nb == 0){
		nbr[0] = '0';
		i++;
	} if (nb < 0){
		nb = -nb;
		my_putchar('-');
	} while (nb != 0){
		nbr[i] = (nb % 10) + 48;
		nb = nb / 10;
		i++;
	}
	nbr[i] = 0;
	nbr = my_revstr(nbr);
	nbr[i] = 0;
	my_putstr(nbr);
	return (0);
}
