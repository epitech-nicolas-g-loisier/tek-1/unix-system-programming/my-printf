/*
** EPITECH PROJECT, 2017
** Lib: convertisseur
** File description:
** convert decimal to binary
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "my.h"

char	*convert_to_binary(int nb)
{
	long	i  = 0;
	char	*result = malloc(sizeof(char) * 64);

	while (nb != 0){
		result[i] = (nb % 2) + 48;
		nb = nb / 2;
		i++;
	}
	result[i] = 0;
	return (my_revstr(result));
}
