/*
** EPITECH PROJECT, 2017
** lib for my_printf
** File description:
** flags for my_printf
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "my.h"

int	special_flag(va_list ap)
{
	int	i = 0;
	char	*str = va_arg(ap, char*);

	while (str[i] != 0){
		if (str[i] < 32 || str[i] >= 127){
			my_putchar('\\');
			my_putstr(convert_to_octal(str[i]));
		} else
			my_putchar(str[i]);
		i++;
	}
	return (0);
}

char    *convert_to_address(unsigned long nb)
{
	long    power = 0;
	long    tmp_nb = nb;
	char    *result = malloc(sizeof(char) * 100);
	int     i = 0;

	while (nb != 0){
		power = 0;
		tmp_nb = nb;
		while (tmp_nb >= 16){
			tmp_nb = tmp_nb / 16;
			power++;
		} if (power == 0){
			result[0] = find_hexa_low(0, nb);
			nb = 0;
		} else {
			result[power] = find_hexa_low(power, nb);
			nb = nb % my_compute_power_it(16, power);
		}
		i++;
	}
	result[i - 1] = 'f';
	result[i] = 0;
	return (my_revstr(result));
}

int	put_addres(va_list ap)
{
	long	nb = va_arg(ap, long);

	if (nb == 0)
		write(2, "(nil)", 5);
	else {
		my_putstr("0x");
		my_putstr(convert_to_address(nb));
	}
	return (0);
}
