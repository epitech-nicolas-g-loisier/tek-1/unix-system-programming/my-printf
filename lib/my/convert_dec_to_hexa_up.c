/*
** EPITECH PROJECT, 2017
** Lib: convertisseur
** File description:
** convert decimal to hexadecimal with "ABCDEF"
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "my.h"

char	find_hexa_up(long power, long nb)
{
	long	tmp = 0;
	long	div = my_compute_power_it(16, power);
	char	res;
	char	*chara = "0123456789ABCDEF";

	tmp = nb / div;
	res = chara[tmp];
	return (res);
}

char	*convert_to_exa_up(int nb)
{
	long	power = 0;
	long	tmp_nb = nb;
	char	*result = malloc(sizeof(char) * 64);

	result = full_res(result);
	while (nb != 0){
		power = 0;
		tmp_nb = nb;
		while (tmp_nb >= 16){
			tmp_nb = tmp_nb / 16;
			power++;
		} if (power == 0){
			result[0] = find_hexa_up(0, nb);
			nb = 0;
		} else {
			result[power] = find_hexa_up(power, nb);
			nb = nb % my_compute_power_it(16, power);
		}
	}
	return (clear_zero(my_revstr(result)));
}
