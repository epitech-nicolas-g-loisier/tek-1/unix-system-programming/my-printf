/*
** EPITECH PROJECT, 2017
** my_putchar
** File description:
** write letter
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "my.h"

int	my_putchar_p(va_list ap)
{
	char c = (char) va_arg(ap, int);

	write (1, &c, 1);
	va_end(ap);
	return (0);
}
