/*
** EPITECH PROJECT, 2017
** My Printf
** File description:
** Putstr with va_list in arg
*/

#include "my.h"

int	my_putstr_p(va_list ap)
{
	int	i = 0;
	char	*str = va_arg(ap, char*);

	while (str[i] != 0){
		my_putchar(str[i]);
		i = i + 1;
	}
	return (0);
}
