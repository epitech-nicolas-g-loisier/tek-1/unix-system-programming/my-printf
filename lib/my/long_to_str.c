/*
** EPITECH PROJECT, 2017
** Lib
** File description:
** convert lont to char*
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "my.h"

char	*long_to_str(long nb)
{
	char	*nbr = "0";
	int	i = 0;
	long	tmp = nb;

	if (nb == 0)
		return ("0");
	while (tmp != 0 && tmp != -1){
		tmp = tmp / 10;
		i++;
	}
	nbr = malloc(sizeof(char) * (i + 2));
	if (nb < 0){
		nbr[0] = '-';
		nb = -nb;
	} else
		i--;
	nbr[i + 1] = 0;
	while (nb != 0){
		nbr[i] = (nb % 10) + 48;
		nb = nb / 10;
		i--;
	}
	return (nbr);
}
