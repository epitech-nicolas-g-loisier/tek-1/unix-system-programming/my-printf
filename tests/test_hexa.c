/*
** EPITECH PROJECT, 2017
** My Printf
** File description:
** Test for print hexa
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include "my.h"

void redirect_all_std_hexa(void)
{
	cr_redirect_stdout();
	cr_redirect_stderr();
}

Test(my_printf, print_hexa_low, .init = redirect_all_std_hexa)
{
	int	c = 15;

	my_printf("%x",c);
	cr_assert_stdout_eq_str("f");
}

Test(my_printf, print_hexa_up, .init = redirect_all_std_hexa)
{
	int	c = 15;

	my_printf("%X",c);
	cr_assert_stdout_eq_str("F");
}
