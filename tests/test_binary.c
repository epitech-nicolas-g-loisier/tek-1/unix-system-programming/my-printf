/*
** EPITECH PROJECT, 2017
** My Printf
** File description:
** Test for print binary
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include "my.h"

void redirect_all_std_bin(void)
{
	cr_redirect_stdout();
	cr_redirect_stderr();
}

Test(my_printf, print_one_binary, .init = redirect_all_std_bin)
{
	int	c = 2;

	my_printf("%b",c);
	cr_assert_stdout_eq_str("10");
}

Test(my_printf, print_long_binary, .init = redirect_all_std_bin)
{
	int	b = 99934;

	my_printf("%b", b);
	cr_assert_stdout_eq_str("11000011001011110");
}
