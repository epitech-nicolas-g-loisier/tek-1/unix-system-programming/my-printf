/*
** EPITECH PROJECT, 2017
** My Printf
** File description:
** Test for print octal
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include "my.h"

void redirect_all_std_octal(void)
{
	cr_redirect_stdout();
	cr_redirect_stderr();
}

Test(my_printf, print_one_octal, .init = redirect_all_std_octal)
{
	int	c = 8;

	my_printf("%o",c);
	cr_assert_stdout_eq_str("10");
}

Test(my_printf, print_two_octal, .init = redirect_all_std_octal)
{
	int	c = 62;
	int	b = 999;

	my_printf("%o, %o", c, b);
	cr_assert_stdout_eq_str("76, 1747");
}
