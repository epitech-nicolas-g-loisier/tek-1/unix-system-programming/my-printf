/*
** EPITECH PROJECT, 2017
** My Printf
** File description:
** Test for print char
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include "my.h"

void redirect_all_std_char(void)
{
	cr_redirect_stdout();
	cr_redirect_stderr();
}

Test(my_printf, print_one_char, .init = redirect_all_std_char)
{
	char	c = 'A';

	my_printf("%c",c);
	cr_assert_stdout_eq_str("A");
}

Test(my_printf, print_two_char, .init = redirect_all_std_char)
{
	char	c = 'A';
	char	b = 'B';

	my_printf("%c, %c", c, b);
	cr_assert_stdout_eq_str("A, B");
}
