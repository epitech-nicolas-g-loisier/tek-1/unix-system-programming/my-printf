/*
** EPITECH PROJECT, 2017
** My Printf
** File description:
** Test for print int
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include "my.h"

void redirect_all_std_int(void)
{
	cr_redirect_stdout();
	cr_redirect_stderr();
}

Test(my_printf, print_one_int, .init = redirect_all_std_int)
{
	int	c = 10;

	my_printf("%d",c);
	cr_assert_stdout_eq_str("10");
}

Test(my_printf, print_two_int, .init = redirect_all_std_int)
{
	int	b = 999999;

	my_printf("%i", b);
	cr_assert_stdout_eq_str("999999");
}
