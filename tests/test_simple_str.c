/*
** EPITECH PROJECT, 2017
** My Printf
** File description:
** Test for print only str
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include "my.h"

void redirect_all_std(void)
{
	cr_redirect_stdout();
	cr_redirect_stderr();
}

Test(my_printf, simple_string, .init = redirect_all_std)
{
	my_printf("Hello world\n");
	cr_assert_stdout_eq_str("Hello world\n");
}
