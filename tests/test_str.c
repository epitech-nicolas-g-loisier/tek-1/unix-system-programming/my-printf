/*
** EPITECH PROJECT, 2017
** My Printf
** File description:
** Test for print str
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include "my.h"

void redirect_all_std_str(void)
{
	cr_redirect_stdout();
	cr_redirect_stderr();
}

Test(my_printf, print_one_str, .init = redirect_all_std_str)
{
	char	*c = "Hello";

	my_printf("%s",c);
	cr_assert_stdout_eq_str("Hello");
}

Test(my_printf, print_str_empty, .init = redirect_all_std_str)
{
	char	*c = "";

	my_printf("%s",c);
	my_printf("hello");
	cr_assert_stdout_eq_str("hello");
}

Test(my_printf, print_two_str, .init = redirect_all_std_str)
{
	char	*c = "I love chocolatine";
	char	*b = "I hate segfault";

	my_printf("%s, %s", c, b);
	cr_assert_stdout_eq_str("I love chocolatine, I hate segfault");
}
